# Must source environment variables
# RDS_PASSWORD
# RDS_HOSTNAME

FROM ruby:2.4.1

# Set DB environment variables
ENV RDS_DATABASE=cdvs
ENV RDS_USERNAME=developer
ENV RDS_PORT=5432

# Run Puma in production
ENV RAILS_ENV=production

# Set up application directory
RUN mkdir /cardiovascular
WORKDIR /cardiovascular

# Install JavaScript runtime
RUN apt-get update
RUN apt-get install -y build-essential nodejs

# Only run bundler if Gemfile changes
COPY ./Gemfile /cardiovascular
COPY ./Gemfile.lock /cardiovascular
RUN bundle install

# Copy application files
COPY . /cardiovascular

# Compile assets
RUN bin/rails assets:precompile

# Start server
CMD /cardiovascular/run_server.sh