require "application_system_test_case"

class CardiogramsTest < ApplicationSystemTestCase
  setup do
    @cardiogram = cardiograms(:one)
  end

  test "visiting the index" do
    visit cardiograms_url
    assert_selector "h1", text: "Cardiograms"
  end

  test "creating a Cardiogram" do
    visit cardiograms_url
    click_on "New Cardiogram"

    fill_in "Authors", with: @cardiogram.authors
    fill_in "Desc", with: @cardiogram.desc
    fill_in "Name", with: @cardiogram.name
    click_on "Create Cardiogram"

    assert_text "Cardiogram was successfully created"
    click_on "Back"
  end

  test "updating a Cardiogram" do
    visit cardiograms_url
    click_on "Edit", match: :first

    fill_in "Authors", with: @cardiogram.authors
    fill_in "Desc", with: @cardiogram.desc
    fill_in "Name", with: @cardiogram.name
    click_on "Update Cardiogram"

    assert_text "Cardiogram was successfully updated"
    click_on "Back"
  end

  test "destroying a Cardiogram" do
    visit cardiograms_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cardiogram was successfully destroyed"
  end
end
