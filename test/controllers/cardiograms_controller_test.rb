require 'test_helper'

class CardiogramsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cardiogram = cardiograms(:one)
  end

  test "should get index" do
    get cardiograms_url
    assert_response :success
  end

  test "should get new" do
    get new_cardiogram_url
    assert_response :success
  end

  test "should create cardiogram" do
    assert_difference('Cardiogram.count') do
      post cardiograms_url, params: { cardiogram: { authors: @cardiogram.authors, desc: @cardiogram.desc, name: @cardiogram.name } }
    end

    assert_redirected_to cardiogram_url(Cardiogram.last)
  end

  test "should show cardiogram" do
    get cardiogram_url(@cardiogram)
    assert_response :success
  end

  test "should get edit" do
    get edit_cardiogram_url(@cardiogram)
    assert_response :success
  end

  test "should update cardiogram" do
    patch cardiogram_url(@cardiogram), params: { cardiogram: { authors: @cardiogram.authors, desc: @cardiogram.desc, name: @cardiogram.name } }
    assert_redirected_to cardiogram_url(@cardiogram)
  end

  test "should destroy cardiogram" do
    assert_difference('Cardiogram.count', -1) do
      delete cardiogram_url(@cardiogram)
    end

    assert_redirected_to cardiograms_url
  end
end
