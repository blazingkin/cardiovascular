require 'test_helper'

class PulseControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get pulse_new_url
    assert_response :success
  end

  test "should get create" do
    get pulse_create_url
    assert_response :success
  end

  test "should get destroy" do
    get pulse_destroy_url
    assert_response :success
  end

end
