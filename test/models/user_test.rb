require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: 'Test User',
                     email: 'valid@email.com',
                     password: 's3cuReP@ss',
                     password_confirmation: 's3cuReP@ss')
  end

  test 'has password' do
    @user.password = ''
    @user.password_confirmation = ''

    assert_not @user.valid?
  end

  test 'passes valid email addresses' do
    valid_emails = %w[test@email.com t.e.st@em.ail.js val_id@email.co.uk ok.ay_email@yes.ss]
    valid_emails.each do |valid_email|
      @user.email = valid_email

      assert @user.valid?
    end
  end

  test 'fails invalid email addresses' do
    invalid_emails = %w[invalid@ema_il.com invalid[AT]email.com invalid@ema+il.co]
    invalid_emails.each do |invalid_email|
      @user.email = invalid_email

      assert_not @user.valid?
    end
  end

  test 'has name' do
    @user.name = ''

    assert_not @user.valid?
  end

  test 'name is not too long' do
    @user.name = 'a' * 65

    assert_not @user.valid?
  end

  test 'email is not too long' do
    @user.email = 'a' * (257 - '@example.com'.length) + '@example.com'

    assert_not @user.valid?
  end
end
