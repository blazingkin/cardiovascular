class AddUserToCardiogram < ActiveRecord::Migration[5.2]
  def change
    add_reference :cardiograms, :user, foreign_key: true
  end
end
