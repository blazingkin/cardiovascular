class AddRepoToCardiogram < ActiveRecord::Migration[5.2]
  def change
    add_column :cardiograms, :repo, :string
  end
end
