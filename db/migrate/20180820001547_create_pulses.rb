class CreatePulses < ActiveRecord::Migration[5.2]
  def change
    create_table :pulses do |t|
      t.string :version

      t.timestamps
    end
  end
end
