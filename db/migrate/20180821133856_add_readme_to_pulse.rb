class AddReadmeToPulse < ActiveRecord::Migration[5.2]
  def change
    add_column :pulses, :readme, :text
  end
end
