class CreateCardiograms < ActiveRecord::Migration[5.2]
  def change
    create_table :cardiograms do |t|
      t.string :name
      t.string :desc
      t.string :authors

      t.timestamps
    end
  end
end
