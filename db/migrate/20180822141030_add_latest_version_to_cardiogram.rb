class AddLatestVersionToCardiogram < ActiveRecord::Migration[5.2]
  def change
    add_column :cardiograms, :latest_version, :string
  end
end
