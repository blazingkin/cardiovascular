# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_22_233831) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "cardiograms", force: :cascade do |t|
    t.string "name"
    t.string "desc"
    t.string "authors"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.string "repo"
    t.string "latest_version"
    t.index ["user_id"], name: "index_cardiograms_on_user_id"
  end

  create_table "comment", force: :cascade do |t|
    t.string "message", null: false
    t.bigint "user_id"
  end

  create_table "email", force: :cascade do |t|
    t.string "email", null: false
    t.bigint "user_id"
    t.string "verkey"
    t.index ["email"], name: "unique_email", unique: true
  end

  create_table "pulses", force: :cascade do |t|
    t.string "version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "cardiogram_id"
    t.text "readme"
    t.index ["cardiogram_id"], name: "index_pulses_on_cardiogram_id"
  end

  create_table "user", force: :cascade do |t|
    t.string "ident", null: false
    t.string "password"
    t.index ["ident"], name: "unique_user", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.date "dob"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "username"
  end

  add_foreign_key "cardiograms", "users"
  add_foreign_key "comment", "\"user\"", column: "user_id", name: "comment_user_id_fkey"
  add_foreign_key "email", "\"user\"", column: "user_id", name: "email_user_id_fkey"
  add_foreign_key "pulses", "cardiograms"
end
