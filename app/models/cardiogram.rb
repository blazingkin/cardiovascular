class Cardiogram < ApplicationRecord
  has_many :pulses, dependent: :destroy

  belongs_to :user

  def to_param
    name
  end
end
