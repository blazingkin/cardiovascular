class User < ApplicationRecord
  validates :username, presence: true,
                       uniqueness: true

  validates :email, presence: true,
                    format: { with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i,
                              message: 'Invalid email.'},
                    length: { maximum: 256 },
                    uniqueness: true

  validates :name,  presence: true,
                    length: { maximum: 64 }

  validates :password, confirmation: true
  has_secure_password

  has_many :cardiograms, dependent: :destroy

  def to_param
    username
  end
end
