class Pulse < ApplicationRecord
  validates :version, uniqueness: { scope: :cardiogram_id }
  
  has_one_attached :tarball

  include Rails.application.routes.url_helpers
  def as_json(a)
    Hash["version": self.version, "url": rails_blob_url(self.tarball, disposition: 'attachment', host: 'https://cdvs.blazingk.in')]
  end

  belongs_to :cardiogram
end