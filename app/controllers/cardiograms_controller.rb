class CardiogramsController < ApplicationController
  before_action :set_cardiogram, only: [:show, :edit, :update, :destroy]

  # GET /cdgs
  # GET /cdgs.json
  def index
    @cardiograms = Cardiogram.all.sort
  end

  # GET /cdg/:name
  # GET /cdg/:name.json
  # GET /cdg/:name/:version
  # GET /cdg/:name/:version.json
  def show
    @cardiogram = Cardiogram.find_by_name(params[:name])
  end

  # GET /cdgs/new
  def new
    @cardiogram = Cardiogram.new
  end

  # GET /cdg/:name/edit
  def edit
  end

  # POST /cdgs
  # POST /cdgs.json
  def create
    @cardiogram = Cardiogram.new(cardiogram_params)

    respond_to do |format|
      if @cardiogram.save
        format.html { redirect_to @cardiogram, notice: 'Cardiogram was successfully created.' }
        format.json { render :show, status: :created, location: @cardiogram }
      else
        format.html { render :new }
        format.json { render json: @cardiogram.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cdg/:name
  # PATCH/PUT /cdg/:name.json
  def update
    respond_to do |format|
      if @cardiogram.update(cardiogram_params)
        format.html { redirect_to @cardiogram, notice: 'Cardiogram was successfully updated.' }
        format.json { render :show, status: :ok, location: @cardiogram }
      else
        format.html { render :edit }
        format.json { render json: @cardiogram.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cdg/:name
  # DELETE /cdg/:name.json
  def destroy
    if current_user == @cardiogram.user
      @cardiogram.destroy

      respond_to do |format|
        format.html { redirect_to cardiograms_url, notice: 'Cardiogram was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to @cardiogram, notice: 'Cardiogram can only be deleted by owner.'}
        format.json { render json: @cardiogram.errors, status: :forbidden }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_cardiogram
    @cardiogram = Cardiogram.find_by_name params[:name]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cardiogram_params
    params.require(:cardiogram).permit(:name, :desc, :authors, :user_id)
  end
end
