class PulsesController < ApplicationController
  def index
    respond_to do |format|
      @cardiogram = Cardiogram.find_by_name params[:name]
      @pulses = Pulse.where cardiogram_id: @cardiogram.id
      format.html { render :index }
      format.json { render json: @pulses}
    end
  end

  def new
    @pulse = Pulse.new
    @cardiogram = Cardiogram.find_by_name params[:name]
  end

  # TODO: Json api !! important !!
  def create
    @pulse = Pulse.new pulse_params

    cardiogram = Cardiogram.find_by_name params[:name]

    respond_to do |format|
      if @pulse.save
        cardiogram.latest_version = @pulse.version
        cardiogram.save
        format.html { redirect_to cardiogram, notice: 'Pulse successfully published.'}
        # TODO: Json response
      else
        format.html { render :new }
        # TODO: Json response
      end
    end
  end

  def destroy
  end

  private

  def pulse_params
    params.require(:pulse).permit(:version, :cardiogram_id, :tarball)
  end
end
