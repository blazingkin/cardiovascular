json.extract! user, :id, :email, :name, :dob, :created_at, :updated_at
json.url user_url(user, format: :json)
