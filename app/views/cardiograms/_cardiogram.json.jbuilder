json.extract! cardiogram, :id, :name, :desc, :authors, :created_at, :updated_at
json.url cardiogram_url(cardiogram, format: :json)
