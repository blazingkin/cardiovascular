Rails.application.routes.draw do
  # This part of Rails is criminally underdeveloped, and I want to help but I
  # am not able to.

  root    'cardiograms#index'

  # Login
  get     '/login',               to: 'sessions#new'
  post    '/login',               to: 'sessions#create'
  delete  '/logout',              to: 'sessions#destroy'

  # Pulses
  get     '/cdg/:name/pls',       to: 'pulses#index',       as: 'pulses'
  get     '/cdg/:name/pls/new',   to: 'pulses#new',         as: 'new_pulse'
  post    '/cdg/:name/pls',       to: 'pulses#create'
  delete  '/cdg/:name/:version',  to: 'pulses#destroy'

  # Cardiograms
  get     '/cdgs',                to: 'cardiograms#index',  as: 'cardiograms'
  post    '/cdgs',                to: 'cardiograms#create'
  get     '/cdgs/new',            to: 'cardiograms#new',    as: 'new_cardiogram'

  get     '/cdg/:name',           to: 'cardiograms#show',   as: 'cardiogram'
  patch   '/cdg/:name',           to: 'cardiograms#update'
  delete  '/cdg/:name',           to: 'cardiograms#destroy',as: 'delete_cardiogram'
  get     '/cdg/:name/:version',  to: 'cardiograms#show',   as: 'cardiogram_version'

  get     '/cdg/:name/edit',      to: 'cardiograms#edit',   as: 'edit_cardiogram'

  # Users
  get     '/users',               to: 'users#index',        as: 'users'
  post    '/users',               to: 'users#create'
  get     '/users/new',           to: 'users#new',          as: 'new_user'

  get     '/user/:name',          to: 'users#show',         as: 'user'
  delete  '/user/:name',          to: 'users#destroy',      as: 'delete_user'

  get     '/user/:name/edit',     to: 'users#edit',         as: 'edit_user'
end
