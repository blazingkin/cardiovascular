#!/bin/sh

# Intended to only run inside a docker container
echo "Active_storage : install"
/cardiovascular/bin/rails active_storage:install

echo "DB : migrate"
/cardiovascular/bin/rails db:migrate

echo "Run the server"
/cardiovascular/bin/rails s