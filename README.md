# cardiovascular

## Environment Information

| Title | Version |
| ------|-------- |
| Ruby | 2.4.1 |
| Rails | 5.2.1 |
| Postgres | 10.4 |

## Configuration

All database credentials are expected to be sourced either through dotenv or
manually from a file containing bindings for the following environment
variables.

```bash
export RDS_DATABASE=[db name]
export RDS_USERNAME=[user/role]
export RDS_PASSWORD=[password]
export RDS_HOSTNAME=[endpoint]
export RDS_PORT=[default: 5432]
```

## Testing

> The test suite is a joke right now.

To run the test suite:

```bash
$ rails test
```

## Deployment

Deploying is currently based on prayer, and only seems to work from my machine,
but don't worry, I'm going to fix that.